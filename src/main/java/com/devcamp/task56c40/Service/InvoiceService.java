package com.devcamp.task56c40.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task56c40.models.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;

    Invoice invoice1 = new Invoice(10, 10.000);
    Invoice invoice2 = new Invoice(11, 10.000);
    Invoice invoice3 = new Invoice(12, 10.000);

    public ArrayList<Invoice> getAllInvoice() {
        ArrayList<Invoice> allInvoice = new ArrayList<>();

        invoice1.setCustomer(customerService.getAllCustomer().get(0));
        invoice2.setCustomer(customerService.getAllCustomer().get(1));
        invoice3.setCustomer(customerService.getAllCustomer().get(2));

        allInvoice.add(invoice1);
        allInvoice.add(invoice2);
        allInvoice.add(invoice3);

        return allInvoice;
    }

    public Invoice fillterIndex(int index){
        
        if(index <= -1 || index > getAllInvoice().size()){
            return null;
        }
        return getAllInvoice().get(index);
    }
}
